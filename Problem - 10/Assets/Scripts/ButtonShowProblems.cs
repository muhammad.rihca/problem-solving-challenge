﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Diagnostics;
using System.ComponentModel;

namespace MyProcessSample
{
    public class ButtonShowProblems : MonoBehaviour
    {
        public int problem;
        public void ProblemShow()
        {

            // Process.Start("Problem - 9.exe","D:\\SekolahAndroid\\Unity 3D\\WorkSpace Learn\\Unity 2019.4.15f1 LTS Dilo - Problem Solving Challenge\\Problem - 9\\Build Problem 9\\Problem - 9.exe");

            Process myProcess = new Process();

            try
            {
                myProcess.StartInfo.UseShellExecute = false;
                // You can start any process, HelloWorld is a do-nothing example.
                myProcess.StartInfo.FileName = "D:\\SekolahAndroid\\Unity 3D\\WorkSpace Learn\\Unity 2019.4.15f1 LTS\\Dilo - Problem Solving Challenge\\Problem - " + problem + "\\Build Problem " + problem + "\\Problem - " + problem + ".exe";
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();
                // This code assumes the process you are starting will terminate itself.
                // Given that is is started without a window so you cannot terminate it
                // on the desktop, it must terminate itself or you can do it programmatically
                // from this application using the Kill method.
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}